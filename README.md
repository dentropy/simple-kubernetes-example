# Example Kubernetes Deployment

``` bash

kubectl apply -f service.yaml

kubectl apply -f deployment.yaml

kubectl get pods

kubectl get services

minikube service flask-test-service --url
```

## How to take everything down

``` bash
kubectl delete -f service.yaml

kubectl delete -f deployment.yaml

```
